<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // creas un array vacio y le mas metiendo los datos 
        $alumnos = array();
        $alumnos[] = "Ramon";
        $alumnos[] = "Jose";
        $alumnos[] = "Pepe";
        $alumnos[] = "Ana";
        
        // creas un segundo array con datos
        $alumnos1 = array("Ramon", "Jose", "Pepe", "Ana");
        
        // recorres el primer array con un for
        for ($c=0; $c<count($alumnos);$c++){
            // imprimes los datos del array $alumno
            echo "$alumnos[$c]<br>";
        }
        
        // recorres el segundo array con un foreach
        foreach ($alumnos1 as $value){
            // imprimes los datos del array $alumno1
            echo "$value<br>";
        }
        ?>
    </body>
</html>
