<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            // inicializas las variables
            $alumno1 = "Ramon";
            $alumno2 = "Jose";
            $alumno3 = "Pepe";
            $alumno4 = "Ana";
            
            // imprimes
            echo $alumno1;
            echo $alumno2;
            echo "<br>"; // salto de linea
            echo $alumno3;
            echo "<br>";
            echo $alumno4;
        ?>
        <div>
            <?php
            // imprimes en columna con un solo echo
                echo "$alumno1<br>$alumno2<br>$alumno3<br>$alumno4";
            ?>
        </div>
        
    </body>
</html>
