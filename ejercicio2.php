<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $entero = 10;
            $cadena= "hola";
            $real= 23.6;
            $logico = true;
            
            var_dump($entero); // devuelve la informacion de la variable entero como int
            var_dump($cadena); // devuelve la informacion de la variable cadena como string de tamaño 4
            var_dump($real); // devuelve la informacion de la variable real como float
            var_dump($logico); // devuelve la informacion de la variable logico como boolean
            
            $logico=(int)$logico; // la varable logico guarda la conversion a int de logico
            $entero=(float)$entero; //la varable entero guarda la conversion a float de entero
            settype($logico, "int");
            
            var_dump($entero); // devuelve la informacion de la variable enterpo como float
            var_dump($cadena); // devuelve la informacion de la variable cadena como string de tamaño 4
            var_dump($real); // devuelve la informacion de la variable real como float
            var_dump($logico); // devuelve la informacion de la variable logico como int 1
            
        ?>
    </body>
</html>
