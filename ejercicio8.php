<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // creas un array con datos
        $v5 =array(0, 1, 0, 1, 0);
        
        // recorres el array con un for y lo imprimes
        foreach ($v5 as $key => $value){
            echo "<br>\$v5[$key]=$value";
        }
        
        // recorres el array con un foreach y lo imprimes
        for ($c=0; $c<count($v5);$c++){
            echo "<br>\$v5[$c]=$v5[$c]";
        }
        ?>
    </body>
</html>
