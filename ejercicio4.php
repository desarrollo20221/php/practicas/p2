<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // preparas las variables
        $s;
        $j;
        $k;
        $i;
        // inicializa la variable $a y $s
        $a = 2.345;
        
        $s = 0;
        
        // haces un bucle con la variable $j
        for ($j = 1; $j <= 4; $j++){
            // imprimes $j
            echo $j;
            // preguntas si $j es par
            // y si es verdad que lo sume a la variable $s
            if ($j % 2 == 0){
                $s+=$j;
            }
        }
        
        printf("<br>%d", $j);
        
        //muestra la informacion de la variable $s
        var_dump($s);
        
        // inicializas la varible $i
        $i = 10;
        // haces un bucle para cuando $i valaga mas que cero
        while ($i > 0){
            $i = $i - 1;
        }
        
        // imprimes
        echo $i;
        echo gettype($i) . "<br>";
        print_r($i);
        var_dump($a);
        ?>
    </body>
</html>
